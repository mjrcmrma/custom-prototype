'use strict';

(function () {
	var array = ['hola', 'ño', 'simona'],
		trim = '      Trim habilitado      ';

	if (array.indexOf('hola') > -1) {
		alert('indexOf is enabled');
	}

	alert(trim.trim());

})();